const long sensorPin1 = A0;
const long sensorPin2 = A1;
const long sensorPin3 = A2;
const long sensorPin4 = A3;
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"


Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);



long sensorValue1,sensorValue2,sensorValue3,sensorValue4;

int OptoL1;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
}

void loop() {
  // put your main code here, to run repeatedly:
  sensorValue1 = analogRead(sensorPin1);
  sensorValue2 = analogRead(sensorPin2);
  sensorValue3 = analogRead(sensorPin3);
  sensorValue4 = analogRead(sensorPin4);
  Serial.print("Reading: ");
  Serial.print(sensorValue1);
  Serial.print(",");
  Serial.print(sensorValue2);
  Serial.print(",");
  Serial.print(sensorValue3);
  Serial.print(",");
  Serial.print(sensorValue4);
  Serial.print(",");
  Serial.println(OptoL1);
  leftMotor->setSpeed(250);
  rightMotor->setSpeed(250);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  delay(2000);
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
  delay(2000);
}
