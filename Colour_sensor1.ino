int sensor1b = 0;
int sensor2b = 0;
int sensor3b = 0;
int sensorDiff;
static int sensorHighb = 0;
static int sensorLowb = 1023;
double sensorAvgb;
int sensor1r = 0;
int sensor2r = 0;
int sensor3r = 0;
static int sensorHighr = 0;
static int sensorLowr = 1023;
double sensorAvgr;

void setup() {
  // put your setup code here, to run once:
  pinMode(A0, INPUT);
  pinMode(A1,INPUT);
  Serial.begin(9600);

}

void loop() {
  char colour[] = "NONE";
  // put your main code here, to run repeatedly:
  sensor1b = analogRead(A0);
  delay(50);
  sensor2b = analogRead(A0);
  delay(50);
  sensor3b = analogRead(A0);
  sensorAvgb = (sensor1b + sensor2b + sensor3b) / 3;
  if(sensorAvgb > sensorHighb){
    sensorHighb = sensorAvgb;
    
  }
  if(sensorAvgb< sensorLowb){
    sensorLowb = sensorAvgb;
  }
  Serial.print("BLUE");
  Serial.print(" ");
  Serial.print(sensorLowb);
  Serial.print(" ");
  Serial.print(sensorHighb);
  Serial.print(" ");
  Serial.print(sensorAvgb);

  sensor1r = analogRead(A1);
  delay(50);
  sensor2r = analogRead(A1);
  delay(50);
  sensor3r = analogRead(A1);
  sensorAvgr = (sensor1r + sensor2r + sensor3r) / 3;
  if(sensorAvgr > sensorHighr){
    sensorHighr = sensorAvgr;
    
  }
  if(sensorAvgr< sensorLowr){
    sensorLowr = sensorAvgr;
  }
  Serial.print("       ");
  Serial.print("RED");
  Serial.print(" ");
  Serial.print(sensorLowr);
  Serial.print(" ");
  Serial.print(sensorHighr);
  Serial.print(" ");
  Serial.print(sensorAvgr);
  Serial.print("  ");
  sensorDiff = sensorAvgr - sensorAvgb;

  Serial.print(sensorDiff);
  Serial.print("   ");
  Serial.print("I SEE ");
  if(sensorDiff<360){
    Serial.print("BLUE");
  }
  if(sensorDiff>450){
    Serial.print("RED");
  }
  Serial.println(" ");
  
  
  
// Add and LED, add red sensor, test line detection
}
