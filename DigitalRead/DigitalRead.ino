const int sensorPin2 = 4;
const int sensorPin3 = 5;
const int sensorPin4 = 6;
const int sensorPin5 = 7;

long sensorValue2,sensorValue3,sensorValue4,sensorValue5;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  sensorValue2 = digitalRead(sensorPin2);
  sensorValue3 = digitalRead(sensorPin3);
  sensorValue4 = digitalRead(sensorPin4);
  sensorValue5 = digitalRead(sensorPin5);
  Serial.print("Reading: ");
  Serial.print(sensorValue2);
  Serial.print(",");
  Serial.print(sensorValue3);
  Serial.print(",");
  Serial.print(sensorValue4);
  Serial.print(",");
  Serial.println(sensorValue5);
}
