const int sensorPin = A1;
const int batteryPin = A2;
long sensorValue;
long batteryValue;
long sensorProportion;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(100);
  sensorValue = analogRead(sensorPin);
  batteryValue = analogRead(batteryPin);
  sensorProportion = 100*batteryValue/sensorValue;
  Serial.print("Reading: ");
  Serial.println(sensorProportion);
}
