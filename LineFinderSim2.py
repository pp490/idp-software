# Created by Eric (Huiyuan Zhang) for Part IB Integrated Design Project
# Michaelmas 2020-21

# import turtle
import numpy as np
import math
import time
import shapely
from shapely.geometry import LineString,Point
import matplotlib.pyplot as plt
import matplotlib
from descartes import PolygonPatch


line = LineString([(0, 0), (1, 1), (0, 2), (2, 2), (3, 1), (1, 0)])
# # plt.plot(*line.xy)
# # plt.show()
line2 = line.buffer(distance=0.2,cap_style=2,join_style=2)
patch1 = PolygonPatch(line2)
fig, ax = plt.subplots(num=None, figsize=(16, 12), dpi=80, facecolor='w', edgecolor='k')
plt.xlim([-5,20])
plt.ylim([-5,15])
ax.add_patch(patch1)
plt.show()


# Module characteristics
motor_lag = 0.1 # s
motor_error = 5 # Percentage random error
sensor_lag = {'optoswitch':0.05,'ultrasound':0.3,'ir_long':0.15,'ir_short':0.1} # s
motor_power = 60.0 # mm/s

# Assuming the motors center at [0,0] in a twin driving wheel configuration, spaced 200mm apart, with +x being axis of motion
wheel_dist = 200 # mm
sensor_location_opto = [[0,15],[30,0],[0,-15]]#,[-50,0]]

# Set up simulator parameters
simul_time = 50 # s
simul_speed = 5 #scaling factor for simulation run speed
step_time = 0.1 # s
mapNum = 2 # Refer to related functions

# Initialize
# turtle.screensize(1000,750) # 1 "pixel" corresponding to 1 mm in real-world
# robot = turtle.Turtle()
# mapper = turtle.Turtle()
# mapper.hideturtle()
# mapper.speed(0)
# robot.speed(0)
pi = math.pi

# Create the optoReading(location,mapNum,radius) method # Line width is 18
def mapInfo(mapNum=0):
    if mapNum == 0: # Straignt line
        map_straight_seg = [[-500,0,500,0]] # [Start X, Start Y, End X, End Y]
        map_circular_seg = [] # [Start X, Start Y, Radius +CCW -CW, Start Angle, Span Angle]; abs(Radius) > lineWidth/2
        
    elif mapNum == 1: # Circle
        map_straight_seg = []
        map_circular_seg = [[0,0,300,0,360]]
        
    elif mapNum == 2: # Rectangle with slightly rounded corners # NEW TECHNIQUE
        map_straight_seg = []
        map_circular_seg = []
        line = LineString([(0,0),(600,0),(600,400),(0,400),(0,0)])
        line_buffered = line.buffer(9)

    elif mapNum == 3: # Rectangle with straight corners
        map_straight_seg = [[-400,0,191,0],[200,0,200,291],[200,300,-391,300],[-400,300,-400,9]]
        map_circular_seg = []

    elif mapNum == 4: # Y-shape intersection
        map_straight_seg = []
        map_circular_seg = []

    elif mapNum == 5: # T-shape intersection
        map_straight_seg = []
        map_circular_seg = []

    elif mapNum == 6: # Red target box
        map_straight_seg = []
        map_circular_seg = []

    elif mapNum == 7: # Custom
        map_straight_seg = [[-500,0,500,0],[-500,18,500,18]]
        map_circular_seg = [[0,0,100,20,60]]
    return line_buffered #map_straight_seg, map_circular_seg

def optoReading(sensor_real_x,sensor_real_y,line_buffered,radius=1,lineWidth=18):
    line_buffered = mapInfo(mapNum)
    total_proportion = 0.0
    sensor_real_pos = Point(sensor_real_x,sensor_real_y)
    sensor_real_range = sensor_real_pos.buffer(radius)
    if sensor_real_range.intersects(line_buffered):
        total_proportion = 1
    total_proportion = min(total_proportion,1)
    return line_buffered


# The control algorithm to test
def control(sensor_reading_opto,motor_power,l1,r1,override=False):
    # 4 sensors, but only first 3 at the moment
    if max(sensor_reading_opto)==0:
        left_motor = right_motor = -0.2 * motor_power
    else:# Find place with strongest signal
##        max_locations = [i for i,d in enumerate(sensor_reading_opto) if d==max(sensor_reading_opto)]
##        if max_locations == [1]:
##            left_motor = right_motor = 0.5 * motor_power
##        else: # The actual algorithm
##            max_loc_avg = sum(max_locations)*1.0/len(max_locations)
##            left_motor = motor_power * (0.25+0.5*max_loc_avg)
##            right_motor = motor_power * (0.25+0.5*(2-max_loc_avg))
        avg_loc = sum(np.array(sensor_reading_opto)*np.array([0,1,2]))/sum(sensor_reading_opto)
        left_motor = motor_power * (0.25+0.25*(avg_loc-1))
        right_motor = motor_power * (0.25+0.25*(1-avg_loc))
        l2=5*left_motor-4*l1
        r2=5*right_motor-4*r1
    return left_motor,right_motor,l2,r2

# Draw map
patch1 = PolygonPatch(mapInfo(mapNum))
fig, ax = plt.subplots(num=None, figsize=(16, 12), dpi=80, facecolor='w', edgecolor='k')
plt.xlim([-5,20])
plt.ylim([-5,15])
ax.add_patch(patch1)
plt.show()
# Main loop
timestamps = np.linspace(0,simul_time,round(simul_time/step_time)+1)
robot.pu()
robot.setposition(-400,0)
robot.setheading(15)
robot.pd()
l1=r1=0

for timestamp in timestamps:
    pos = robot.position()
    hdg = robot.heading()
    sensor_reading_opto = []
    line_buffered = mapInfo(mapNum)
    for sensor_loc in sensor_location_opto:
        # Calculate sensor positions (not taking crosstalk into account)
        sensor_real_x = pos[0]+math.cos(pi*hdg/180)*sensor_loc[0]-math.sin(pi*hdg/180)*sensor_loc[1]
        sensor_real_y = pos[1]+math.cos(pi*hdg/180)*sensor_loc[1]+math.sin(pi*hdg/180)*sensor_loc[0]
        # Obtain sensor readings
        sensor_reading_opto.append(1000*np.array(optoReading(sensor_real_x,sensor_real_y,line_buffered,radius=5,lineWidth=18)))
    # Apply control algorithm
    #print(sensor_reading_opto)
    l1,r1,left_motor,right_motor = control(sensor_reading_opto,motor_power,l1,r1,override=False)
    #print(left_motor,right_motor)
    if left_motor == right_motor:
        robot.forward(left_motor*step_time)
    else:
        # Calculate the radius of curvature
        turnAngle = (right_motor - left_motor)*step_time/wheel_dist # positive if turning left, or CCW
        pathLength = (right_motor + left_motor)*step_time/2
        robot.circle(pathLength/turnAngle,abs(turnAngle*180/pi))
    time.sleep(step_time/simul_speed)


