#include <Arduino_LSM6DS3.h>
//include <string>
#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
//include <NavigationFunctions.h>
//include <SensorFunctions.h>
//include <PackageFunctions.h>

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

int OptoFwd,OptoLeft,OptoRight,OptoAft,MotorPowerLeft,MotorPowerRight,mode,prevMode,split,turnAngle=0;
float turnFactor = 0.0; //-1.0 for max left, 1.0 for max right
float speedFactor = 0.0; //1.0 for max fwd, -1.0 for max reverse
float currDist,totalDist,totalTurn = 0.0; // length of current split and complete journey
float prevTurn,newTurn = 0.0;
float MotorMultiplierLeft,MotorMultiplierRight;
float MotorLengthMultiplier = 0.3; // (Length/Speed)/MLM = time for move, time*MLM*Speed=Length
float MotorAngleMultiplier = 0.2; // (Angle/LRSpeedDiff)/MAM = time for turn
unsigned long startTime,prevTime,currTime;

const int sensorPin1 = A2;
const int sensorPin2 = A0;
const int sensorPin3 = A1;
const int sensorPin4 = A3;

// how the map behaves
//int mapInfo [12] = {5,6,6,5,6,6,5,7,5,4,6,6};//7 is for the smooth junction leading to the second drop zone
// rough distance guide between each measured point (mm)
//int distInfo [12] = {650,940,800,300,0,0,0,0,0,}

void motorTranslate(float turnFactor, float speedFactor){
  MotorMultiplierLeft = (turnFactor+speedFactor)/(abs(turnFactor)+abs(speedFactor));
  MotorMultiplierRight = (-turnFactor+speedFactor)/(abs(turnFactor)+abs(speedFactor));
  MotorPowerLeft = 255 * MotorMultiplierLeft;
  MotorPowerRight = 255 * MotorMultiplierRight;
  // Left motor control
  if (MotorMultiplierLeft>0) {
    leftMotor->run(FORWARD);
    leftMotor->setSpeed(MotorPowerLeft);
  } else if (MotorMultiplierLeft<0) {
    leftMotor->run(BACKWARD);
    leftMotor->setSpeed(int (-1*MotorPowerLeft));
  } else {
    leftMotor->run(RELEASE);
  }
  // Right motor control
  if (MotorMultiplierRight>0) {
    rightMotor->run(FORWARD);
    rightMotor->setSpeed(MotorPowerRight);
  } else if (MotorMultiplierRight<0) {
    rightMotor->run(BACKWARD);
    rightMotor->setSpeed(int (-1*MotorPowerRight));
  } else {
    rightMotor->run(RELEASE);
  }
}

// Initialise: mode 0
// Gentle left/right turn, straight line: mode 1
// Left-turn/junction-ready: mode 4
// Right-turn/junction-ready: mode 5
// Target box ready: mode 6
// Exit straight function: mode 8

// Straight: mode 1, 4, 5, 6; returns 8
void straight (int optoFwd, int optoLeft, int optoRight, int optoAft, float& turnFactor, float& speedFactor, int& mode, float& prevTurn) {
  if (optoFwd + optoAft == 0) {
    // The center-line sensors are both off track.
    // Go back
    speedFactor = -0.2;
  } else if (mode == 1 or mode == 6) {
    turnFactor = (0.2*optoFwd - 0.5*optoAft)*(optoLeft - optoRight);// /(optoLeft + optoFwd*0.5 + optoAft*0.5 + optoRight);
    speedFactor = 0.8;
    if (mode == 6 and optoLeft == 1 and optoRight == 1) {
      mode = 8;
      turnFactor = 0;
      speedFactor = 0;
    }
  } else if (mode == 4) {
    //In the main code, turn slight left and move forward to make the left opto unblocked, before calling this function!
    if(optoLeft==1) {
      turnFactor = 0;
      speedFactor = 0;
      mode = 8;
    } else {
      turnFactor = 0.5*optoRight-0.2*optoFwd;
      speedFactor = 0.3;
    }
  } else if (mode == 5) {
    //In the main code, turn slight right and move forward until the right opto is unblocked, before calling this function!
    if (optoRight==1) {
      turnFactor = 0;
      speedFactor = 0;
      mode = 8;
    } else {
      turnFactor = -0.5*optoLeft+0.2*optoFwd;
      speedFactor = 0.3;
    }
  }
  newTurn = turnFactor + 2*(turnFactor-prevTurn);
  prevTurn = prevTurn*0.8+turnFactor*0.2; // exponential MA5
  turnFactor = newTurn;
}

void corner(int turnAngle){ //Positive for CCW, negative for CW
  rightMotor->setSpeed(150);
  leftMotor->setSpeed(150);
  if (turnAngle > 0) {
    rightMotor->run(FORWARD);
    leftMotor->run(BACKWARD);
  } else {
    rightMotor->run(BACKWARD);
    leftMotor->run(FORWARD);
  }
  delay((abs(turnAngle/300))/MotorAngleMultiplier);
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  AFMS.begin();
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
  startTime = millis();
  prevTime = millis();
}

void loop() {
  // set up reference mode at the start
  prevMode = mode;
  turnFactor = 0;
  speedFactor = 0;
  // read out sensor data
  OptoL2 = analogRead(sensorPin1) < 600;
  OptoL1 = analogRead(sensorPin2) < 600;
  OptoR1 = analogRead(sensorPin3) < 600;
  OptoR2 = analogRead(sensorPin4) < 600;

  // find line and adjust
  if (mode == 0) {
    // Find line leading out of start box
    mode = 1; // do not know the departure rules yet
  } else if (mode == 1 or mode == 4 or mode == 5 or mode == 6) {
    // straight, turn prep, or box sensing
    straight (OptoFwd, OptoLeft, OptoRight, OptoAft, turnFactor, speedFactor, mode, prevTurn);
    if (mode == 1 and currDist > 100) {
      mode = 4;
    }
  } else if (mode == 8) {
    // check location for required action
    currDist = 0;
    split += 1;
    
    // for testing purposes, just for now
    //
    //
    
    rightMotor->setSpeed(200);
    leftMotor->setSpeed(100);
    rightMotor->run(FORWARD);
    leftMotor->run(BACKWARD);
    delay((90/300)/MotorAngleMultiplier);
    rightMotor->run(RELEASE);
    leftMotor->run(RELEASE);
    mode = 1;
  }

  // move
  motorTranslate(turnFactor,speedFactor);
  
  // sense whether robot is nearing junction, and change to mode 4/5/6
  // ?????
  // testing
  
  // sense mode change
  if (mode == 4 and prevMode != 4) {
    // turn slight left and move forward
    rightMotor->setSpeed(100);
    rightMotor->run(FORWARD);
    delay((15/100)/MotorAngleMultiplier);
    rightMotor->run(RELEASE);
  } else if (mode == 5 and prevMode != 5) {
    // turn slight right and move forward
    leftMotor->setSpeed(100);
    leftMotor->run(FORWARD);
    delay((15/100)/MotorAngleMultiplier);
    leftMotor->run(RELEASE);
  }

  // Could remove this line
  delay(50);
  
  // Record data
  currTime = millis();
  currDist += (currTime-prevTime) * MotorLengthMultiplier * speedFactor;
  totalDist += (currTime-prevTime) * MotorLengthMultiplier * speedFactor;
  totalTurn += (currTime-prevTime) * MotorAngleMultiplier * turnFactor;
  Serial.print("  Total Distance: ");
  Serial.print(totalDist);
  Serial.print("  Split Distance: ");
  Serial.print(currDist);
  Serial.print("  Split Number: ");
  Serial.print(split);
  Serial.print("  Total Turn: ");
  Serial.println(totalTurn);
  prevTime = millis();
}
