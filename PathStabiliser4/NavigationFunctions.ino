void turnRight(int timeDelay) {
  leftMotor->setSpeed(standardLeftSpeed);
  rightMotor->setSpeed(standardRightSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  delay(timeDelay);
  stopMotors(); 
}

void halfTurn() {
  float ultrasoundDistanceBack = getUltrasound(trigPin1, echoPin1);
  while (ultrasoundDistanceBack >= 10) {
    leftMotor->setSpeed(150);
    rightMotor->setSpeed(141);
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
    ultrasoundDistanceBack = getUltrasound(trigPin1, echoPin1);
  }
}

void turnLeft(int timeDelay) {
  leftMotor->setSpeed(standardLeftSpeed);
  rightMotor->setSpeed(standardRightSpeed);
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  delay(timeDelay);
  stopMotors();
}

void goForwards(int timeDelay) {
  leftMotor->setSpeed(standardLeftSpeed);
  rightMotor->setSpeed(standardRightSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  delay(timeDelay);

}

void goBackwards(int timeDelay) {
  leftMotor->setSpeed(standardLeftSpeed);
  rightMotor->setSpeed(standardRightSpeed);
  leftMotor->run(BACKWARD);
  rightMotor->run(BACKWARD);
  delay(timeDelay);
}

void stopMotors() {
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
}

void fixedBlueDeliveryMechanism() {
  goForwards(1000);                         //get out of tunnel
  turnRight(timeForNinetyDegreeTurn);       //turn right
  goForwards(2700);                         //get to end of red line box
  turnRight(timeForNinetyDegreeTurn);       //turn right 90 degrees
  goBackwards(1400);                        //go backwards until back on block
  openBluePincers(blueServo);               //open blue pincers
  goForwards(timeToTravelBrickLength);      //move forwards one brick length to deliver blue box
  closeBluePincers(blueServo);              //close blue pincers
  digitalWrite(blueLED1, LOW);              //turn off one LED
  turnRight(timeForNinetyDegreeTurn);       //turn right 90 degrees
  goForwards(timeToTravelParallelToRedLine);//go forwards parallel to red line
  turnLeft(timeForNinetyDegreeTurn);        //turn left 90 degrees
  goBackwards(timeToTravelBrickLength);     //go backwards one brick length
  openBluePincers(blueServo);               //open blue pincers and deliver blue box
  digitalWrite(blueLED2, LOW);              //turn off second LED
  goForwards(400);                          //get away from target delivery square
  closeBluePincers(blueServo);              //close blue pincers
  turnRight(timeForNinetyDegreeTurn);       //right turn 90 degrees
  goForwards(timeToGetBackToStartBox);      //go forwards back into start box
}
