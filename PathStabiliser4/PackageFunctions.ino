void openRedPincers(Servo servo) {
  int pos;
  for (pos = redServoPos; pos <= 90; pos += 1) {
    servo.write(pos);
    delay(25);
  }
  redServoPos = pos;
}

void openBluePincers(Servo servo) {
  int pos;
  for (pos = blueServoPos; pos <= 90; pos += 1) {
    servo.write(pos);
    delay(25); 
  }
  blueServoPos = pos;
}

void closeRedPincers(Servo servo) {
  int pos;
  for (pos = redServoPos; pos >= 0; pos -= 1) { 
    servo.write(pos); 
    delay(25);  
  }
  redServoPos = pos;  
}

void closeBluePincers(Servo servo) {
  int pos;
  for (pos = blueServoPos; pos >= 0; pos -= 1) {
    servo.write(pos);
    delay(25);        
  }
  blueServoPos = pos;   
}

void deliverRedPackage() {
  goForwards(timeToTravelChassisLength);        //go forwards so back pincer over delivery square
  stopMotors;
  turnRight(timeForOneEightyTurn);              //orient red loading area over delivery square
  openRedPincers(redServo);                     //drop off red package
  if (!allRedPackagesDelivered) {               //if this is first red package
    goBackwards(timeToTravelBrickLength);       //pick up second package in outermost slot
  } else {                                      //else
    goBackwards(timeToTravelBrickLength*2);     //drive off
  }
  stopMotors();
  closeRedPincers(redServo);                    //close pincers
  goBackwards(timeToTravelBrickLength);         //drive off to avoid subsequent turn knocking delivered package
  stopMotors;
  turnRight(timeForOneEightyTurn);
  packagesDelivered += 1;
}

void targetBoxFunction() {
  if (allPackagesCollected) {
    if (!allRedPackagesDelivered) {
      stopMotors();
      deliverRedPackage();
      if (digitalRead(redLED1) == 0) digitalWrite(redLED2, 0);
      else digitalWrite(redLED1, 0);
      goForwards(timeToLeaveDeliverySquare); 
    }
  }      
}

void collectPackageFunction(float ultrasoundDistanceFront) {
    if (ultrasoundDistanceFront < 10) {
    stopMotors();
    if (bluePackageDetected()) {
      //pick up blue package in back pincer     
       
      turnRight(timeForOneEightyTurn);
      openBluePincers(blueServo);
      goBackwards(timeToTravelBrickLength);
      stopMotors();
      closeBluePincers(blueServo);

      if (digitalRead(blueLED1) == LOW) digitalWrite(blueLED1, HIGH);
      else digitalWrite(blueLED2, HIGH);
     
      turnRight(timeForOneEightyTurn);         
    } else {
      // pick up red package at front pincer     
      openRedPincers(redServo);
      goForwards(timeToTravelBrickLength);
      stopMotors();
      closeRedPincers(redServo);
      if (digitalRead(redLED1) == LOW) digitalWrite(redLED1, HIGH);
      else digitalWrite(redLED2, HIGH);    
    }
    packagesCollected += 1; 
  }
}
