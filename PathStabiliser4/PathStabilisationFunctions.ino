void motorTranslate(float turnFactor, float speedFactor){
  
  MotorMultiplierLeft = (2.0*turnFactor+speedFactor)/(2.0*turnFactor*(2*(turnFactor>0)-1)+1);
  MotorMultiplierRight = (-2.0*turnFactor+0.94*speedFactor)/(2.0*turnFactor*(2*(turnFactor>0)-1)+1);
  MotorPowerLeft = 255 * MotorMultiplierLeft;
  MotorPowerRight = 255 * MotorMultiplierRight;
  Serial.print(" MML:");
  Serial.print(MotorMultiplierLeft);
  Serial.print(" MMR:");
  Serial.print(MotorMultiplierRight);
  Serial.print(" MPL:");
  Serial.print(MotorPowerLeft);
  Serial.print(" MPR:");
  Serial.print(MotorPowerRight);
  // Left motor control
  if (MotorPowerLeft>0) {
    leftMotor->run(FORWARD);
    leftMotor->setSpeed(MotorPowerLeft);
  } else if (MotorPowerLeft<0) {
    leftMotor->run(BACKWARD);
    leftMotor->setSpeed(-1*MotorPowerLeft);
  } else {
    leftMotor->run(RELEASE);
  }
  // Right motor control
  if (MotorPowerRight>0) {
    rightMotor->run(FORWARD);
    rightMotor->setSpeed(MotorPowerRight);
  } else if (MotorPowerRight<0) {
    rightMotor->run(BACKWARD);
    rightMotor->setSpeed(-1*MotorPowerRight);
  } else {
    rightMotor->run(RELEASE);
  }
}

// Initialise: mode 0
// Gentle left/right turn, straight line: mode 1
// Left-turn/junction-ready: mode 4
// Right-turn/junction-ready: mode 5
// Target box ready: mode 6
// Exit straight function: mode 8

// Straight: mode 1, 4, 5, 6; Sharp split 7; returns 8
void straight (int OptoL2, int OptoL1, int OptoR1, int OptoR2, float& turnFactor, float& speedFactor, int& mode, float& prevTurn) {
  if (OptoL2 + OptoL1 + OptoR1 + OptoR2 == 0) {
    // All sensors are on blackoff track.
    // Go back
    speedFactor = -0.7;
  } else if (mode == 1 or mode == 6 or mode == 7) {
    turnFactor = - 0.35*OptoL2 - 0.15*OptoL1 + 0.15*OptoR1 + 0.35*OptoR2;
    speedFactor = 1-0.3*(OptoL2+OptoR2);
    if ((mode == 6 or mode == 7) and OptoL2 + OptoL1 + OptoR1 + OptoR2 >= 3) {
      mode = 8;
      turnFactor = 0;
      speedFactor = 0;
    }
  } else if (mode == 4) {
    //In the main code, turn slight left and move forward to make the left opto unblocked, before calling this function!
    if(OptoL2==1) {
      turnFactor = 0;
      speedFactor = 0;
      mode = 8;
    } else {
      turnFactor = -0.4*OptoL1 + 0.4*OptoR2;// + 0.1*OptoR1;
      speedFactor = 0.7;
    }
  } else if (mode == 5) {// or mode == 7) {
    //In the main code, turn slight right and move forward until the right opto is unblocked, before calling this function!
    if (OptoR2==1) {
      turnFactor = 0;
      speedFactor = 0;
      mode = 8;
    } else {
      turnFactor = -0.4*OptoL2 + 0.4*OptoR1;//- 0.1*OptoL1;
      speedFactor = 0.7;
    }
  }
  newTurn = turnFactor - 0.1*prevTurn;//0.15
  prevTurn = prevTurn*0.85+turnFactor*0.15; // exponential form MA6 //0.85 0.15
  turnFactor = newTurn;
}

void corner(int turnAngle){ //Positive for CCW, negative for CW
  rightMotor->setSpeed(194);
  leftMotor->setSpeed(206);
  if (turnAngle > 0) {
    rightMotor->run(BACKWARD);
    leftMotor->run(FORWARD);
  } else {
    rightMotor->run(FORWARD);
    leftMotor->run(BACKWARD);
  }
  delayTime = abs(turnAngle)/(400.0*MotorAngleMultiplier);
  Serial.print(" (New) delay:");
  Serial.print(delayTime);
  Serial.print(" (Old) delay:");
  Serial.print((sqrt(turnAngle*turnAngle)/400)/MotorAngleMultiplier);
  delay(delayTime);
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
}

void pathStabilisation() {
  // set up reference mode at the start
  prevMode = mode;
  turnFactor = 0;
  speedFactor = 0;
  // read out sensor data
  OptoL2 = analogRead(sensorPin1) < 700;
  OptoL1 = analogRead(sensorPin2) < 580;
  OptoR1 = analogRead(sensorPin3) < 700;
  OptoR2 = analogRead(sensorPin4) < 580;
  Serial.print("Opto L-R:");
  Serial.print(OptoL2);
  Serial.print(OptoL1);
  Serial.print(OptoR1);
  Serial.print(OptoR2);
  // find line and adjust
  if (mode == 0) {
    // Find line leading out of start box
    mode = 1; // do not know the departure rules yet /////////////////////////
  } else if (mode == 1 or mode == 4 or mode == 5 or mode == 6) {
    // straight, turn prep, or box sensing
    straight (OptoL2, OptoL1, OptoR1, OptoR2, turnFactor, speedFactor, mode, prevTurn);
    if (mode == 1 and (currDist > (distInfo[split]-150))) {//////Straight Mode switch
      mode = mapInfo[split];//4 for testing
    }
  }

  // move
  motorTranslate(turnFactor,speedFactor);
  
  // sense mode change
  if (mode == 4 and prevMode != 4) {
    // turn slight left and move forward
    rightMotor->setSpeed(194);
    rightMotor->run(FORWARD);
    delay((10/200)/MotorAngleMultiplier);
    rightMotor->run(RELEASE);
  } else if (mode == 5 and prevMode != 5) {
    // turn slight right and move forward
    leftMotor->setSpeed(206);
    leftMotor->run(FORWARD);
    delay((10/200)/MotorAngleMultiplier);
    leftMotor->run(RELEASE);
  }
  if (mode == 8) {
    // check map
    if (mapInfo[split] == 4) {
      rightMotor->setSpeed(200); // move forward by ~70mm
      leftMotor->setSpeed(200);
      rightMotor->run(FORWARD);
      leftMotor->run(FORWARD);
      delay(500);
      rightMotor->run(RELEASE);
      leftMotor->run(RELEASE);
      corner(-90);
    } else if (mapInfo[split] == 5 and split != 4) { //////there is a problem for this part
      rightMotor->setSpeed(200); // move forward by ~70mm
      leftMotor->setSpeed(200);
      rightMotor->run(FORWARD);
      leftMotor->run(FORWARD);
      delay(500);
      rightMotor->run(RELEASE);
      leftMotor->run(RELEASE);
      corner(90);
    } else if (mapInfo[split] == 6) {
      targetBoxFunction();
      rightMotor->setSpeed(200); // move forward by ~70mm
      leftMotor->setSpeed(200);
      rightMotor->run(FORWARD);
      leftMotor->run(FORWARD);
      delay(500);
      rightMotor->run(RELEASE);
      leftMotor->run(RELEASE);
    } else if (mapInfo[split] == 7) {
      if (split != 0){
        // turn right at the smooth bend
        rightMotor->setSpeed(100);
        leftMotor->setSpeed(250);
        rightMotor->run(FORWARD);
        leftMotor->run(FORWARD);
        delay((90/150)/MotorAngleMultiplier);
        rightMotor->run(RELEASE);
        leftMotor->run(RELEASE);
      } else {
        rightMotor->setSpeed(100);
        rightMotor->run(FORWARD);
        delay((10/100)/MotorAngleMultiplier);
        rightMotor->run(RELEASE);
      }
    }
    // update
    currDist = 0;
    split += 1;
    totalTurn += -90;
    mode = 1;
  }

  // see if package is ahead
  
  float ultrasoundDistanceFront = getUltrasound(trigPin2, echoPin2);  

  if (!allPackagesCollected) {
    collectPackageFunction(ultrasoundDistanceFront);
  }
  
  // Slow time when testing
  //delay(50);

  currTime = millis();

  
  // Record data and blink LED 
  if (currTime-amberTime>500){
    amberTime = currTime;
    digitalWrite(amberLED,HIGH);
  } else if (currTime-amberTime>250){
    digitalWrite(amberLED,LOW);
  }
  
  currDist += (currTime-prevTime) * MotorLengthMultiplier * speedFactor;
  totalDist += (currTime-prevTime) * MotorLengthMultiplier * speedFactor;
  totalTurn += (currTime-prevTime) * MotorAngleMultiplier * (MotorPowerLeft-MotorPowerRight);
  if (totalDist < 0) {
    totalDist = 0;
  }
  if (currDist < 0) {
    currDist = 0;
  }
  Serial.print(" Mode:");
  Serial.print(mode);
  Serial.print(" SF:");
  Serial.print(speedFactor);
  Serial.print(" TF:");
  Serial.print(turnFactor);
  Serial.print(" LTot:");
  Serial.print(totalDist);
  Serial.print(" LLap:");
  Serial.print(currDist);
  Serial.print(" Lap:");
  Serial.print(split);
  Serial.print(" Tot Turn:");
  Serial.println(totalTurn);
  prevTime = millis();

  if (split == 7 && currDist >= 1000) ending = true;
  
}
