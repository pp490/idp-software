/* Main navigation algorithm */

#include <Arduino_LSM6DS3.h>
#include <Servo.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"

/*global variables */

int packagesCollected = 0;
int packagesDelivered = 0;
bool allPackagesCollected = packagesCollected == 4;
bool allRedPackagesDelivered = packagesDelivered > 1;
bool allPackagesDelivered = packagesDelivered == 4;
Servo redServo;
Servo blueServo;
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

/* path stabilisation */
int OptoL2,OptoL1,OptoR1,OptoR2,MotorPowerLeft,MotorPowerRight,mode,prevMode,split,turnAngle=0;
float turnFactor = 0.0; //-1.0 for max left, 1.0 for max right
float speedFactor = 0.0; //1.0 for max fwd, -1.0 for max reverse
float currDist,totalDist,totalTurn = 0.0; // length of current split and complete journey
float prevTurn,newTurn,delayTime = 0.0;
float MotorMultiplierLeft,MotorMultiplierRight;
const float MotorLengthMultiplier = 0.3; // (Length/Speed)/MLM = time for move, time*MLM*Speed=Length
const float MotorAngleMultiplier = 0.00018; // (Angle/LRSpeedDiff)/MAM = time for turn
unsigned long startTime,prevTime,currTime,amberTime;

/* pin setup */
const int buttonPin = A5;
const int sensorPin1 = A3;
const int sensorPin2 = A2;
const int sensorPin3 = A1;
const int sensorPin4 = A0;

const int blueLED1 = 2;
const int blueLED2 = 3;
const int amberLED = 4;
const int redLED1 = 5;
const int redLED2 = 6;

const int trigPin2 = 7;
const int echoPin2 = 8;

const int colourPin = 11;

const int echoPin1 = 12;
const int trigPin1 = 13;

/* ultrasound variables */
long duration;
long distance;

/* navigation variables */
int timeToLeaveDeliverySquare = 500;
int timeToGetBackToStartBox = 2500*5;
int timeToTravelChassisLength = 1400;
int timeToTravelBrickLength = 175;
int timeToTravelParallelToRedLine = 2550;
int timeForNinetyDegreeTurn = 1250;
int timeForOneEightyTurn = 2500;

int standardLeftSpeed = 206;
int standardRightSpeed = 194;

int redServoPos = 90;
int blueServoPos = 90;

bool ending = false;

/* navigation map variables */
int mapInfo [9] = {5,6,6,5,6,6,5,7,5};//7 is for the smooth junction leading to the second drop zone
// rough distance guide between each measured point (mm)
int distInfo [9] = {2000,1120,1120,2250,1120,1120,2250,850,750};///the first figure should be 2000


void setup() {
  // put your setup code here, to run once:
  pinMode(buttonPin, INPUT);
  pinMode(colourPin, INPUT);
  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1,INPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2,INPUT);
  pinMode(redLED1, OUTPUT);
  pinMode(redLED2, OUTPUT);
  pinMode(blueLED1, OUTPUT);
  pinMode(blueLED2, OUTPUT);
  pinMode(amberLED, OUTPUT);
  
  Serial.begin(9600);
  AFMS.begin();
  stopMotors();
  redServo.attach(9);
  blueServo.attach(10);  
  startTime = millis();
  prevTime = millis();
  amberTime = millis();
  while (digitalRead(buttonPin) == LOW) delay(100);
  closeRedPincers(redServo);
  closeBluePincers(blueServo);  
}

void loop() {

  // end case

  if (ending) {
    fixedBlueDeliveryMechanism();
    stopMotors();
    digitalWrite(amberLED, LOW);
    return;
  }
  
  pathStabilisation();

}
