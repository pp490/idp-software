float getUltrasound(int trigPin, int echoPin) {
  digitalWrite(trigPin,LOW);
  delay(50);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin,LOW);
  duration = pulseIn(echoPin,HIGH);
  distance = duration*340/20000;
  return distance;
}

bool bluePackageDetected() {
  delay(100);                                       
  return (digitalRead(colourPin) == LOW);           //LOW reading indicates blue package
}
