/* Main navigation algorithm */

#include <Arduino_LSM6DS3.h>
#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <vec2d.h>

/* global variables */
int packagesCollected = 0;
int packagesDelivered = 0;
bool allPackagesCollected = packagesCollected == 4;
bool allRedPackagesDelivered = packagesDelivered > 1;
bool allPackagesDelivered = packagesDelivered == 4;
Servo redServo;
Servo blueServo;
Adafruit_MotorShield motorShield1 = Adafruit_MotorShield();
Adafruit_MotorShield motorShield2 = Adafruit_MotorShield();
Adafruit_DCMotor* leftMotor = motorShield1.getMotor(1);
Adafruit_DCMotor* rightMotor = motorShield2.getMotor(2);

/* pin setup */
const int colourPin = 12;
const int trigPin1 = 8;
const int trigPin2 =10;
const int echoPin1 = 9;
const int echoPin2 = 11;
const int lineSensor1 = A3;
const int lineSensor2 = A2;
const int lineSensor3 = A1;
const int lineSensor4 = A0;
const int redLED1 = 5;
const int redLED2 = 6;
const int blueLED1 = 2;
const int blueLED2 = 3;
const int amberLED = 4;

/* line sensing variables */
long sensorValue;
long batteryValue;
long sensorProportion;

/* ultrasound variables */
long duration;
long distance;

/* colour sensing */
int sensor1b = 0;
int sensor2b = 0;
int sensor3b = 0;
int sensorDiff;
double sensorAvgb;

/* navigation variables */
int timeToLeaveDeliverySquare = 500;
int timeToGetBackToStartBox = 2500*5;
int forwardMovementTime = 500;
int timeToTravelHalfCarLength = 800;
int timeToTravelBrickLength = 175;
int timeToTravelParallelToRedLine = 2550;

int timeForNinetyDegreeTurn = 1300;
int timeForOneEightyTurn = 2550;

bool start = true;
int sidewaysTJuncCount = 0;
int standardSpeed = 255;
int balanceFactor = 0.71;

int leftSpeed = 198;
int rightSpeed = 202;


void setup() {
  Serial.begin(9600);
    
  motorShield1.begin();
  motorShield2.begin();

  leftMotor->setSpeed(leftSpeed);
  rightMotor->setSpeed(rightSpeed);
}


void loop() {
  goForwards(5000);
  stopMotors();
  turnRight(timeForNinetyDegreeTurn);
  goForwards(5000);
  stopMotors();
  turnRight(timeForNinetyDegreeTurn);
  goForwards(1000);
  stopMotors();
}


void goForwards(int timeDelay) {
  leftMotor->setSpeed(leftSpeed);
  rightMotor->setSpeed(rightSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  digitalWrite(amberLED, HIGH);
  delay(timeDelay);

}

void stopMotors() {
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
  digitalWrite(amberLED, 0);
}

void turnRight(int timeDelay) {
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  digitalWrite(amberLED, HIGH);
  delay(timeDelay);
  stopMotors(); 
}
