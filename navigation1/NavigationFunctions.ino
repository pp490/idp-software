void turnRight(int timeDelay) {
  leftMotor->setSpeed(standardSpeed*balanceFactor);
  rightMotor->setSpeed(standardSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  analogWrite(amberLED, 1023);
  delay(timeDelay);
  stopMotors(); 
}

void turnLeft(int timeDelay) {
  leftMotor->setSpeed(standardSpeed*balanceFactor);
  rightMotor->setSpeed(standardSpeed);
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  analogWrite(amberLED, 1023);
  delay(timeDelay);
  stopMotors();
}

void tiltLeft() {
  leftMotor->setSpeed(standardSpeed*(balanceFactor-0.05));
  rightMotor->setSpeed(standardSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  analogWrite(amberLED, 1023);
  delay(50);
}

void tiltRight() {
  leftMotor->setSpeed(standardSpeed*(balanceFactor+0.05));
  rightMotor->setSpeed(standardSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  analogWrite(amberLED, 1023);
  delay(50);
}

void goForwards(int timeDelay) {
  leftMotor->setSpeed(standardSpeed*balanceFactor);
  rightMotor->setSpeed(standardSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  analogWrite(amberLED, 1023);
  delay(timeDelay);

}

void goBackwards(int timeDelay) {
  leftMotor->setSpeed(standardSpeed*balanceFactor);
  rightMotor->setSpeed(standardSpeed);
  leftMotor->run(BACKWARD);
  rightMotor->run(BACKWARD);
  analogWrite(amberLED, 1023);
  delay(timeDelay);
}

void stopMotors() {
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
  analogWrite(amberLED, 0);
}
