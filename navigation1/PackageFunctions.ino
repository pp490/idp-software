void openPincers(Servo servo) {
  int pos = 0;
  for (pos = 0; pos <= 90; pos += 1) {
    // in steps of 1 degree
    servo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
}

void closePincers(Servo servo) {
  int pos = 90;
  for (pos = 90; pos <= 0; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    servo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
}

void deliverRedPackage() {
  goForwards(timeToTravelHalfCarLength); //go forwards 0.175m so back pincer over delivery square
  stopMotors;
  
  turnRight(180); //orient red loading area over delivery square and drop off
  openPincers(redServo);

  if (!allRedPackagesDelivered) {
    goBackwards(timeToTravelBrickLength);
  } else {
    goBackwards(timeToTravelBrickLength*2);
  }
  stopMotors();
  closePincers(redServo);
  goBackwards(timeToTravelBrickLength);
  stopMotors;
  
  turnRight(180);

  packagesDelivered += 1;
}

void deliverBluePackage() {
  stopMotors();
  openPincers(blueServo);
  
  goForwards(timeToTravelBrickLength); //go forwards 0.02m, brick length
  stopMotors();
  
  closePincers(blueServo);
  
  packagesDelivered += 1;
}

void deliverBluePackages() {
  stopMotors();
  turnLeft(timeForNinetyDegreeTurn);
  goForwards(timeToTravelHalfCarLength);
  deliverBluePackage();
  analogWrite(blueLED1, 0);
  goForwards(timeToTravelParallelToRedLine);
  deliverBluePackage();
  analogWrite(blueLED2, 0);
}

void startOrientationOfBlockDetection() {
  int count = 0;
  
}

float getAcceleration() {
  float x, y, z;
  if (IMU.accelerationAvailable()) {
    IMU.readAcceleration(x, y, z);
  } else {
    x = 0; y = 0; z = 0;
  }
  return x, y, z;
}
