bool getLineSensorReading(int sensorPin) {
  return digitalRead(sensorPin) == 1;
}

float getUltrasound() {
  digitalWrite(trigPin,LOW);
  delay(50);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin,LOW);
  duration = pulseIn(echoPin,HIGH);
  distance = duration*340/20000;
  return distance;
}

bool bluePackageDetected() {
  //returns true if blue, false otherwise
  
  //digitalWrite(7, LOW);
  //digitalWrite(9, LOW);
  sensor1b = digitalRead(colourPin);
  delay(50);
  sensor2b = digitalRead(colourPin);
  delay(50);
  sensor3b = digitalRead(colourPin);
  sensorAvgb = (sensor1b + sensor2b + sensor3b) / 3;
  return(sensorAvgb == 1);
}
