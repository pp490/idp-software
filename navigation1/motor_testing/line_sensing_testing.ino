#include <Arduino_LSM6DS3.h>
#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>

/* global variables */
Servo redServo;
Servo blueServo;
Adafruit_MotorShield motorShield1 = Adafruit_MotorShield();
Adafruit_MotorShield motorShield2 = Adafruit_MotorShield();
Adafruit_DCMotor* leftMotor = motorShield1.getMotor(1);
Adafruit_DCMotor* rightMotor = motorShield2.getMotor(2);

/* pin setup */
const int frontLineSensor = 5;
const int leftLineSensor = 6;
const int rightLineSensor = 7;
const int backLineSensor = 8;
const int trigPin = 3;
const int echoPin = 4;
const int redLED1 = A0;
const int redLED2 = A1;
const int amberLED = 11;

/* line sensing variables */
long sensorValue;
long batteryValue;
long sensorProportion;

/* navigation variables */
float timeToLeaveDeliverySquare = 500;
float timeToGetBackToStartBox = 2500*5;
float forwardMovementTime = 500;
float timeToTravelHalfCarLength = 800;
float timeToTravelBrickLength = 175;
float timeToTravelParallelToRedLine = 2550;

float timeForNinetyDegreeTurn = 1300;
float timeForOneEightyTurn = 2550;

int sidewaysTJuncCount = 0;
int rightCount = 0;

int speed = 150;

void setup() {
  
  pinMode(frontLineSensor, INPUT);
  pinMode(leftLineSensor, INPUT);
  pinMode(rightLineSensor, INPUT);
  pinMode(backLineSensor, INPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin,INPUT);
  
  Serial.begin(9600);
  
  motorShield1.begin();
  motorShield2.begin();

  redServo.attach(9);
  blueServo.attach(10);

  leftMotor->setSpeed(speed*0.7);
  rightMotor->setSpeed(speed);

}

void loop() {

  // looped sensor readings
  
  bool lineSensorFront = getLineSensorReading(frontLineSensor);
  bool lineSensorLeft = getLineSensorReading(leftLineSensor);
  bool lineSensorRight = getLineSensorReading(rightLineSensor);
  bool lineSensorBack = getLineSensorReading(backLineSensor);

  float ultrasoundDistance = getUltrasound();

  // navigation

if (lineSensorFront && lineSensorBack && lineSensorLeft && lineSensorRight) {
    if (allPackagesCollected) {
      if (!allRedPackagesDelivered) {
        
        //deliver red package
        
        stopMotors();
        deliverRedPackage();
        goForwards();
        delay(timeToLeaveDeliverySquare); //get out of delivery square
        
      } else {
        //deliver blue packages
       
        deliverBluePackages();
      }
    } else {
      //get out of delivery square, continuing on line     
      goForwards();
      delay(timeToLeaveDeliverySquare);
    }
  }

}
