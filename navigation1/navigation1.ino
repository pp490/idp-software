/* Main navigation algorithm */

#include <Arduino_LSM6DS3.h>
#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <vec2d.h>

/* global variables */
int packagesCollected = 0;
int packagesDelivered = 0;
bool allPackagesCollected = packagesCollected == 4;
bool allRedPackagesDelivered = packagesDelivered > 1;
bool allPackagesDelivered = packagesDelivered == 4;
Servo redServo;
Servo blueServo;
Adafruit_MotorShield motorShield1 = Adafruit_MotorShield();
Adafruit_MotorShield motorShield2 = Adafruit_MotorShield();
Adafruit_DCMotor* leftMotor = motorShield1.getMotor(1);
Adafruit_DCMotor* rightMotor = motorShield2.getMotor(2);

/* pin setup */
const int colourPin = 2;
const int trigPin = 3;
const int echoPin = 4;
const int frontLineSensor = 5;
const int leftLineSensor = 6;
const int rightLineSensor = 7;
const int backLineSensor = 8;
const int redLED1 = A0;
const int redLED2 = A1;
const int blueLED1 = A2;
const int blueLED2 = A3;
const int amberLED = 11;

/* line sensing variables */
long sensorValue;
long batteryValue;
long sensorProportion;

/* ultrasound variables */
long duration;
long distance;

/* colour sensing */
int sensor1b = 0;
int sensor2b = 0;
int sensor3b = 0;
int sensorDiff;
double sensorAvgb;

/* navigation variables */
int timeToLeaveDeliverySquare = 500;
int timeToGetBackToStartBox = 2500*5;
int forwardMovementTime = 500;
int timeToTravelHalfCarLength = 800;
int timeToTravelBrickLength = 175;
int timeToTravelParallelToRedLine = 2550;

int timeForNinetyDegreeTurn = 1300;
int timeForOneEightyTurn = 2550;

bool start = true;
int sidewaysTJuncCount = 0;
int standardSpeed = 255;
int balanceFactor = 0.71;

void setup() {
  
  pinMode(frontLineSensor, INPUT);
  pinMode(leftLineSensor, INPUT);
  pinMode(rightLineSensor, INPUT);
  pinMode(backLineSensor, INPUT);
  pinMode(colourPin, INPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin,INPUT);
  
  Serial.begin(9600);
  
  motorShield1.begin();
  motorShield2.begin();

  redServo.attach(9);
  blueServo.attach(10);

  leftMotor->setSpeed(standardSpeed*balanceFactor);
  rightMotor->setSpeed(standardSpeed);


}

void loop() {
  
  // looped sensor readings
  
  bool lineSensorFront = getLineSensorReading(frontLineSensor);
  bool lineSensorLeft = getLineSensorReading(leftLineSensor);
  bool lineSensorRight = getLineSensorReading(rightLineSensor);
  bool lineSensorBack = getLineSensorReading(backLineSensor);

  float ultrasoundDistance = getUltrasound();

  //end case

  if (allPackagesDelivered) {
    goForwards(timeToGetBackToStartBox);
    stopMotors();
    return;
  }
  
  // start case

  else if (start) {
    delay(5000);
    goForwards(2500);
    start = false;
  }
  
  //come across delivery square
  
  else if (lineSensorFront && lineSensorBack && lineSensorLeft && lineSensorRight) {
    if (allPackagesCollected) {
      if (!allRedPackagesDelivered) {
        
        //deliver red package
        
        stopMotors();
        deliverRedPackage();
        if (analogRead(redLED1) == 0) analogWrite(redLED2, 0);
        else analogWrite(redLED1, 0);
        goForwards(timeToLeaveDeliverySquare); //get out of delivery square
        
      } else {
        //deliver blue packages
       
        deliverBluePackages();

      }
    } else {
      //get out of delivery square, continuing on line     
      goForwards(timeToLeaveDeliverySquare); //get out of delivery square
    }
  }

  // picking up boxes

  else if (!allPackagesCollected){
    if (ultrasoundDistance < 0.02) {
      stopMotors();
      startOrientationOfBlockDetection();
      bool isBluePackage = bluePackageDetected();
      if (isBluePackage) {
        //pick up blue package in back pincer     
        
        turnRight(timeForOneEightyTurn);
        openPincers(blueServo);
        goForwards(timeToTravelBrickLength);
        stopMotors();
        closePincers(blueServo);

        if (analogRead(blueLED1) == 0) analogWrite(blueLED1, 1023);
        else analogWrite(blueLED2, 1023);
        
        turnRight(timeForOneEightyTurn);  
           
      } else {
        // pick up red package at front pincer     
        openPincers(redServo);
        goForwards(timeToTravelBrickLength);
        stopMotors();
        closePincers(redServo);

        if (analogRead(redLED1) == 0) analogWrite(redLED1, 1023);
        else analogWrite(redLED2, 1023);
        
      }
      packagesCollected += 1; 
    }
    goForwards(0); 

  }  

  // "T-junction" to the right
    
  else if (!lineSensorLeft && lineSensorRight && lineSensorFront && lineSensorBack) {
    if (allPackagesCollected && !allRedPackagesDelivered
        || 2 < sidewaysTJuncCount < 5) {
      goForwards(forwardMovementTime);
    } else {
      stopMotors();
      turnRight(timeForNinetyDegreeTurn);
      goForwards(forwardMovementTime);
    }
    sidewaysTJuncCount += 1;
  }
  
  // normal forward movement
     
  else if (lineSensorFront) {
    goForwards(forwardMovementTime);
  }
  
  // "T-junction" to the left and right  

  else if (lineSensorLeft && lineSensorRight) {
    stopMotors();
    turnRight(timeForNinetyDegreeTurn);
    goForwards(forwardMovementTime);
  }
  
  // left turning
    
  else if (lineSensorLeft) {
    tiltLeft();
  }
  
  // right turning
  
  else if (lineSensorRight) {
    tiltRight();
  }

  else {
    goForwards(0);
  }
}
