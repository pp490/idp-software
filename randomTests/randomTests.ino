#include <Arduino_LSM6DS3.h>
//include <string>
#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
//include <NavigationFunctions.h>
//include <SensorFunctions.h>
//include <PackageFunctions.h>

Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(2);

float delayTime;
const int turnAngle = 90;
const float MotorLengthMultiplier = 0.3; // (Length/Speed)/MLM = time for move, time*MLM*Speed=Length
const float MotorAngleMultiplier = 0.00018; // (Angle/LRSpeedDiff)/MAM = time for turn

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  AFMS.begin();
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
  leftMotor->setSpeed(200);
  rightMotor->setSpeed(200);
}

void loop() {
  rightMotor->setSpeed(194);
  leftMotor->setSpeed(206);
  if (turnAngle > 0) {
    rightMotor->run(BACKWARD);
    leftMotor->run(FORWARD);
  } else {
    rightMotor->run(FORWARD);
    leftMotor->run(BACKWARD);
  }
  delayTime = abs(turnAngle)/(400.0*MotorAngleMultiplier);
  Serial.print(" (Not Implemented) delay time:");
  Serial.print(delayTime);
  Serial.print(" (Implemented) delay time:");
  Serial.print((sqrt(turnAngle*turnAngle)/400)/MotorAngleMultiplier);
  delay((sqrt(turnAngle*turnAngle)/400)/MotorAngleMultiplier);
  rightMotor->run(RELEASE);
  leftMotor->run(RELEASE);
  delay(5000);
}
