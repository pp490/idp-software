#include <Arduino_LSM6DS3.h>
//include <string>
#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
//include <NavigationFunctions.h>
//include <SensorFunctions.h>
//include <PackageFunctions.h>

unsigned long currTime,amberTime;
const int amberLED = 4;

void setup() {
  Serial.begin(9600);
  currTime = millis();
  amberTime = millis();
  pinMode(amberLED, OUTPUT);
}
void loop() {
  currTime = millis();
  if (currTime-amberTime>500){
    amberTime = currTime;
    digitalWrite(amberLED,HIGH);
  } else if (currTime-amberTime>250){
    digitalWrite(amberLED,LOW);
  }
  delay(110);
}
