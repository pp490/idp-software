void turnRight(int timeDelay) {
  leftMotor->setSpeed(206);
  rightMotor->setSpeed(194);
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  moving = true;
  flashAmber();  
  delay(timeDelay);
  stopMotors(); 
}

void halfTurn() {
  float ultrasoundDistanceBack = getUltrasound(trigPin1, echoPin1);
  while (ultrasoundDistanceBack >= 10) {
    Serial.println(ultrasoundDistanceBack);
    leftMotor->setSpeed(206);
    rightMotor->setSpeed(194);
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
    ultrasoundDistanceBack = getUltrasound(trigPin1, echoPin1);
  }
}

void turnLeft(int timeDelay) {
  leftMotor->setSpeed(200);
  rightMotor->setSpeed(200);
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);
  moving = true;
  flashAmber();  
  delay(timeDelay);
  stopMotors();
}

void tiltLeft() {
  leftMotor->setSpeed(standardSpeed*(balanceFactor-0.05));
  rightMotor->setSpeed(standardSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  moving = true;
  flashAmber();
  delay(50);
}

void tiltRight() {
  leftMotor->setSpeed(standardSpeed*(balanceFactor+0.05));
  rightMotor->setSpeed(standardSpeed);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  moving = true;
  flashAmber();  
  delay(50);
}

void goForwards(int timeDelay) {
  leftMotor->setSpeed(200);
  rightMotor->setSpeed(200);
  leftMotor->run(FORWARD);
  rightMotor->run(FORWARD);
  moving = true;
  flashAmber();
  delay(timeDelay);

}

void goBackwards(int timeDelay) {
  leftMotor->setSpeed(200);
  rightMotor->setSpeed(200);
  leftMotor->run(BACKWARD);
  rightMotor->run(BACKWARD);
  moving = true;
  flashAmber();
  delay(timeDelay);
}

void stopMotors() {
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
  moving = false;
}


void flashAmber() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    previousMillis = currentMillis;
    if (amberState == LOW) {
      amberState = HIGH;
    } else {
      amberState = LOW;
    }
    digitalWrite(amberLED, amberState);
  }
}

int openRedPincers(Servo servo) {
  int posit;
  for (posit = redServoPos; posit <= 90; posit += 1) {
    // in steps of 1 degree
    servo.write(posit);
    redServoPos = posit;
    delay(25);
  }
  
}

int openBluePincers(Servo servo) {
  int posit;
  for (posit = blueServoPos; posit <= 90; posit += 1) {
    // in steps of 1 degree
    servo.write(posit);
    blueServoPos = posit;
    delay(25); 
  }
}

int closeRedPincers(Servo servo) {
  int posit;
  for (posit = redServoPos; posit >= 0; posit -= 1) { 
    // in steps of 1 degree
    servo.write(posit); 
    redServoPos = posit;
    delay(25);  
  }
  
}

int closeBluePincers(Servo servo) {
  int posit;
  for (posit = blueServoPos; posit >= 0; posit -= 1) {
    // in steps of 1 degree
    servo.write(posit);
    blueServoPos = posit; 
    delay(25);        
  }
  
}

bool getLineSensorReading(int sensorPin) {
  return analogRead(sensorPin) == 0;
}

float getUltrasound(int trigPin, int echoPin) {
  digitalWrite(trigPin,LOW);
  delay(50);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin,LOW);
  duration = pulseIn(echoPin,HIGH);
  distance = duration*340/20000;
  return distance;
}

bool bluePackageDetected() {
  delay(100);
  return digitalRead(colourPin) == LOW;
}
