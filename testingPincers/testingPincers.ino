/* Main navigation algorithm */

#include <Arduino_LSM6DS3.h>
#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <time.h>

/* global variables */
Servo redServo;
Servo blueServo;
Adafruit_MotorShield motorShield1 = Adafruit_MotorShield();
Adafruit_MotorShield motorShield2 = Adafruit_MotorShield();
Adafruit_DCMotor* leftMotor = motorShield1.getMotor(1);
Adafruit_DCMotor* rightMotor = motorShield2.getMotor(2);

/* pin setup */
const int colourPin = 11;
const int trigPin1 = 13;
const int trigPin2 = 7;
const int echoPin1 = 12;
const int echoPin2 = 8;
const int redLED1 = 5;
const int redLED2 = 6;
const int blueLED1 = 2;
const int blueLED2 = 3;
const int amberLED = 4;
int amberState = LOW;
unsigned long previousMillis = 0;
const long interval = 500;

/* ultrasound variables */
long duration;
long distance;

/* navigation variables */
int timeToLeaveDeliverySquare = 500;
int timeToGetBackToStartBox = 2500*5;
int forwardMovementTime = 500;
int timeToTravelHalfCarLength = 800;
int timeToTravelBrickLength = 500;
int timeToTravelParallelToRedLine = 2550;

int timeForNinetyDegreeTurn = 1250;
int timeForOneEightyTurn = 2500;

bool start = true;
int sidewaysTJuncCount = 0;
int standardSpeed = 255;
int balanceFactor = 0.71;
bool moving = false;

int redServoPos = 90;
int blueServoPos = 90;

void setup() {
  
  pinMode(colourPin, INPUT);
  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1,INPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2,INPUT);
  pinMode(redLED1, OUTPUT);
  pinMode(redLED2, OUTPUT);
  pinMode(blueLED1, OUTPUT);
  pinMode(blueLED2, OUTPUT);
  pinMode(amberLED, OUTPUT);
  
  Serial.begin(9600);
  
  motorShield1.begin();
  motorShield2.begin();

  redServo.attach(10);
  blueServo.attach(9);

  leftMotor->setSpeed(206);
  rightMotor->setSpeed(194);

  while (moving) {
    flashAmber();
  }
  closeRedPincers(blueServo);
  closeBluePincers(redServo);
}

void loop() {
  // looped sensor readings
  
  float ultrasoundDistanceFront = getUltrasound(trigPin2, echoPin2);
  
  if (ultrasoundDistanceFront < 10) {
      stopMotors();      
      if (bluePackageDetected()) {
        //pick up blue package in back pincer     
        halfTurn();
        openBluePincers(blueServo);
        goBackwards(timeToTravelBrickLength);
        stopMotors();
        closeBluePincers(blueServo);

        if (digitalRead(blueLED1) == LOW) digitalWrite(blueLED1, HIGH);
        else digitalWrite(blueLED2, HIGH);
        
        turnRight(timeForOneEightyTurn);  
           
      } else {
        // pick up red package at front pincer     
        openRedPincers(redServo);
        goForwards(timeToTravelBrickLength);
        stopMotors();
        closeRedPincers(redServo);

        if (digitalRead(redLED1) == LOW) digitalWrite(redLED1, HIGH);
        else digitalWrite(redLED2, HIGH);
        
      }
    }
  else {  
    closeBluePincers(blueServo);
    closeRedPincers(redServo);
  }
}
