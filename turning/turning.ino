/* Main navigation algorithm */

#include <Arduino_LSM6DS3.h>
#include <Servo.h>
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <time.h>


/* global variables */
int packagesCollected = 0;
int packagesDelivered = 0;
bool allPackagesCollected = packagesCollected == 4;
bool allRedPackagesDelivered = packagesDelivered > 1;
bool allPackagesDelivered = packagesDelivered == 4;
Servo redServo;
Servo blueServo;
Adafruit_MotorShield motorShield1 = Adafruit_MotorShield();
Adafruit_MotorShield motorShield2 = Adafruit_MotorShield();
Adafruit_DCMotor* leftMotor = motorShield1.getMotor(1);
Adafruit_DCMotor* rightMotor = motorShield2.getMotor(2);

/* pin setup */
const int colourPin = 11;
const int trigPin1 = 13;
const int trigPin2 = 7;
const int echoPin1 = 12;
const int echoPin2 = 8;
const int lineSensor1 = A3;
const int lineSensor2 = A2;
const int lineSensor3 = A1;
const int lineSensor4 = A0;
const int redLED1 = 5;
const int redLED2 = 6;
const int blueLED1 = 2;
const int blueLED2 = 3;
const int amberLED = 4;
int amberState = LOW;
unsigned long previousMillis = 0;
const long interval = 500;

/* line sensing variables */
long sensorValue;
long batteryValue;
long sensorProportion;

/* ultrasound variables */
long duration;
long distance;

/* colour sensing */
int sensor1b = 0;
int sensor2b = 0;
int sensor3b = 0;
int sensorDiff;
double sensorAvgb;

/* navigation variables */
int timeToLeaveDeliverySquare = 500;
int timeToGetBackToStartBox = 2500*5;
int forwardMovementTime = 500;
int timeToTravelHalfCarLength = 800;
int timeToTravelBrickLength = 175;
int timeToTravelParallelToRedLine = 2550;

int timeForNinetyDegreeTurn = 1250;
int timeForOneEightyTurn = 2500;

bool start = true;
int sidewaysTJuncCount = 0;
int standardSpeed = 255;
int balanceFactor = 0.71;
bool moving = false;

void setup() {
  
  pinMode(lineSensor1, INPUT);
  pinMode(lineSensor2, INPUT);
  pinMode(lineSensor3, INPUT);
  pinMode(lineSensor4, INPUT);
  pinMode(colourPin, INPUT);
  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1,INPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2,INPUT);
  pinMode(redLED1, OUTPUT);
  pinMode(redLED2, OUTPUT);
  pinMode(blueLED1, OUTPUT);
  pinMode(blueLED2, OUTPUT);
  pinMode(amberLED, OUTPUT);
  
  Serial.begin(9600);
  
  motorShield1.begin();
  motorShield2.begin();

  redServo.attach(9);
  blueServo.attach(10);

  leftMotor->setSpeed(standardSpeed*balanceFactor);
  rightMotor->setSpeed(standardSpeed);
}


void loop() {
  turnRight(timeForOneEightyTurn);
  delay(1000);
}

void turnRight(int timeDelay) {
  leftMotor->setSpeed(206);
  rightMotor->setSpeed(194);
  leftMotor->run(FORWARD);
  rightMotor->run(BACKWARD);
  moving = true;
  delay(timeDelay);
  stopMotors(); 
}

void stopMotors() {
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
  moving = false;
}
